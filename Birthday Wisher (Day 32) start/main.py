import os
import smtplib
import random
import datetime as dt

with open("quotes.txt", mode="r") as quotes:
    lines = [line.rstrip() for line in quotes]
    random_line = random.choice(lines)


day_of_birth = dt.datetime(year=1996, month=10, day=15)

my_email = os.environ.get('WISHER_EMAIL')
test_email = os.environ.get('TEST_EMAIL')
my_password = os.environ.get('WISHER_PASSWORD')

now = dt.datetime.now()
year = now.year
month = now.month
day_of_week = now.weekday()
if day_of_week == 1:
    with smtplib.SMTP("smtp.gmail.com") as connection:
        connection.starttls()
        connection.login(user=my_email, password=my_password)
        connection.sendmail(
            from_addr=my_email,
            to_addrs=test_email,
            msg=f"Subject:Hello\n\n{random_line}."
        )