import turtle as t
import random

tim = t.Turtle()
t.colormode(255)
def random_color():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    color = (r, g, b)
    return color

########### Challenge 5 - Spirograph ########
def circle():
 t.circle(50)  # draw a semicircle
 t.position()
 t.left(5)
t.speed(50)
full_circle = 360
while not full_circle == 0:
 t.color(random_color())
 circle()
 full_circle -= 5



