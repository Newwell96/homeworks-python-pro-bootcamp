MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
    "money": 0
}

def components(resources):
    water = resources["water"]
    print(f"Water: {water}ml")
    milk = resources["milk"]
    print(f"Milk: {milk}ml")
    coffee = resources["coffee"]
    print(f"Coffe: {coffee}g")
    money = resources["money"]
    print(f"${money}")


def input_cash():
    quarters = int(input("How many quarters?: "))
    dimes = int(input("How many dimes?: "))
    nickles = int(input("How many nickles?: "))
    pennies = int(input("How many pennies?: "))
    input_cash = 0.01 * pennies + 0.05 * nickles + 0.1 * dimes + 0.25 * quarters
    return input_cash


def cost_of_drink(MENU, drink_choice):
    return MENU[drink_choice]["cost"]


def coffe_machine(MENU, resources):

    while True != False:
        continue_game = True
        drink_choice = input("What would you like? (espresso/latte/cappuccino): ").lower()

        if drink_choice=="off":
            return
        elif drink_choice=="report":
            components(resources)


        for machine_component in resources:
            for drink_component in MENU[drink_choice]["ingredients"]:
                if continue_game:
                    if machine_component == drink_component:
                        if resources[drink_component] >= MENU[drink_choice]["ingredients"][machine_component]:
                            resources[drink_component] -= MENU[drink_choice]["ingredients"][machine_component]
                        else:
                            print(f"Sorry there is not enough {drink_component}.")
                            continue_game = False

        if continue_game:
            print("Please insert coins.")

            change_cash = round(input_cash() - cost_of_drink(MENU,drink_choice),2)
            if change_cash >= 0:
                resources["money"] += cost_of_drink(MENU,drink_choice)
                print(f"Here is ${change_cash} in change.")
                print(f"Here is your {drink_choice} ☕. Enjoy!")

            else:
                change_cash = 0
                print(f"Sorry that's not enough money. Money refunded.")


coffe_machine(MENU, resources)
