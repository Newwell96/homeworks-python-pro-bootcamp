from question_model import Question
from data import question_data
from quiz_brain import QuizBrain

question_bank = []

for number in range (len(question_data)):

    #element = Question(question_data[number]["text"],question_data[number]["answer"])
    #question_bank.append([element.text,element.answer])

    question_bank.append(Question(question_data[number]["question"], question_data[number]["correct_answer"]))

quiz = QuizBrain(question_bank)

while  quiz.still_has_questions():
    quiz.next_question()

if quiz.score == 12:
    print("You've comleted the quize")
    print(f"Your final score is:{quiz.score} / {quiz.question_number}.")