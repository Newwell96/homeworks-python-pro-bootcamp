from turtle import Screen
from turtle import Turtle

ALIGNMENT = "center"
FONT = ("Courier", 24, "normal")


class Scoreboard(Turtle):

    def __init__(self):
        super().__init__()
        self.speed("fastest")
        self.color("black")
        self.hideturtle()
        self.penup()
        self.score = 1
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.goto(-200, 260)
        self.write(f"Level: {self.score}", True, align=ALIGNMENT, font=FONT)

    def level_up(self):
        self.score += 1
        self.update_scoreboard()

    def game_over(self):
        self.home()
        self.write("GAME OVER", True, align=ALIGNMENT, font=FONT)



