import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

screen = Screen()
player = Player()
car_manager = CarManager()
scoreboard = Scoreboard()



screen.setup(width=600, height=600)
screen.tracer(0)

screen.listen()
screen.onkey(player.move,"Up")

game_is_on = True
while game_is_on:

    time.sleep(0.1)
    screen.update()

    car_manager.create_car()
    car_manager.move()

    #Player collides with a car and stop the game
    for car in car_manager.all_cars:
        if player.distance(car) <= 20 :
            scoreboard.game_over()
            game_is_on = False

    if player.finish() == True:
        car_manager.acceleration()
        scoreboard.level_up()



screen.exitonclick()