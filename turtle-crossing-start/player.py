from turtle import Turtle
from turtle import Screen
from car_manager import CarManager

STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280

screen = Screen()
car_manager = CarManager()

class Player(Turtle):

    def __init__(self):
        super().__init__()
        screen.tracer(0)
        self.shape("turtle")
        self.penup()
        self.reset_position()
        self.move_speed = 0.1


    def move(self):
        self.forward(MOVE_DISTANCE)

    def reset_position(self):
        if self.pos():
            self.goto(STARTING_POSITION)
            self.setheading(90)

    def finish(self):
        if self.ycor() == FINISH_LINE_Y:
            self.reset_position()
            return True
