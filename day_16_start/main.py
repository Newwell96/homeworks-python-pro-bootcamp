#from turtle import Turtle, Screen
#timmy = Turtle()
#print(timmy)
#timmy.shape("turtle")
#timmy.color("DarkOliveGreen")
#timmy.fd(100)
#my_screen = Screen()
#print(my_screen.canvheight)
#my_screen.exitonclick()

from prettytable import PrettyTable
#table = PrettyTable()
#
#table.add_column("Pikachu","Squirtle","Charmander")
#table.add_column("Electric","Water","Fire")
#print(table)
table = PrettyTable()
table.add_column("Pokemon Name",
["Pikachu","Squirtle","Charmander"])
table.add_column("Type", ["Electric","Water","Fire"])
table.align = "l"
print(table)