BACKGROUND_COLOR = "#B1DDC6"
LANGUAGE_PARAMETERS = "Ariel", 40, "italic"
WORD_PARAMETERS = "Ariel", 60, "bold"

from tkinter import *
import pandas
from random import choice
current_card = {}
list = {}
#Functions
try:
    data = pandas.read_csv("data/words_to_learn.csv")
except FileNotFoundError:
    data = pandas.read_csv("data/french_words.csv")
list = data.to_dict(orient="records")


def next_card():
    global current_card, flip_timer
    window.after_cancel(flip_timer)
    current_card = choice(list)
    canvas.itemconfig(language_text, text="French", fill="black")
    canvas.itemconfig(word_text, text=current_card["French"], fill="black")
    canvas.itemconfig(canvas_image, image=white_background_img)
    window.after(3000, func=flip_card)

def flip_card():
    canvas.itemconfig(canvas_image, image=green_background_img)
    canvas.itemconfig(language_text, text="English", fill="white")
    canvas.itemconfig(word_text, text=current_card["English"], fill="white")


def is_known():
    list.remove(current_card)
    data = pandas.DataFrame(list)
    data.to_csv("data/words_to_learn.csv", index=False)
    next_card()




#GUI Interface
window = Tk()
window.title("Flashy")
window.config(padx=50, pady=50, background=BACKGROUND_COLOR)

flip_timer = window.after(3000, func=flip_card)

canvas = Canvas(width=800, height=526, highlightthickness=0, background=BACKGROUND_COLOR)
white_background_img = PhotoImage(file="images/card_front.png")
green_background_img = PhotoImage(file="images/card_back.png")
canvas_image = canvas.create_image(400, 263, image=white_background_img)
canvas.grid(row=0, column=0, columnspan=2)

language_text = canvas.create_text(400,150,text="French", fill="black", font=LANGUAGE_PARAMETERS)

word_text = canvas.create_text(400,263,text="trouve", fill="black", font=WORD_PARAMETERS)

next_card()

right_button_image = PhotoImage(file="images/right.png")
right_button = Button(image=right_button_image, highlightthickness=0, command=is_known)
right_button.grid(row=1, column=0)

wrong_button_image = PhotoImage(file="images/wrong.png")
wrong_button = Button(image=wrong_button_image, highlightthickness=0, command=next_card)
wrong_button.grid(row=1, column=1)


window.mainloop()