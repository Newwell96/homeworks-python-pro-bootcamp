import requests

OWM_Endpoint = "https://api.openweathermap.org/data/2.5/forecast"

parameters = {
    "lat": 37.461810,
    "lon": -119.949800,
    "appid": "55b69e726293fc84ce0093a2190afdaf",
    "units": "metric",
    "lang": "ru",
    "cnt": 4
}

response = requests.get(url=OWM_Endpoint, params=parameters)
response.raise_for_status()
weather_data = response.json()


for num in range(0,4):
    if weather_data["list"][num]["weather"][0]["id"] < 700:
        print("Bring an Umbrella")
        break

