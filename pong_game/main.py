import turtle
from turtle import Turtle, Screen
from paddle import Paddle
from ball import Ball
from scoreboard import Scoreboard
import time

screen = Screen()
r_paddle = Paddle((350, 0))
l_paddle = Paddle((-350, 0))
ball = Ball()
scoreboard = Scoreboard()


screen.bgcolor("black")
screen.setup(width=800, height=600)
screen.title("Ping Pong")


screen.listen()
screen.onkey(l_paddle.up,"w")
screen.onkey(l_paddle.down,"s")

screen.onkey(r_paddle.up,"Up")
screen.onkey(r_paddle.down,"Down")

the_game = True
while the_game:

    time.sleep(ball.move_speed)
    screen.update()
    ball.move()

    if ball.ycor() > 280 or ball.ycor() < -280:
        ball.y_bounce()


    if ball.distance(r_paddle) < 50 and ball.xcor() > 340 or ball.distance(l_paddle) < 50 and ball.xcor() < -340:
        ball.x_bounce()
        ball.move_speed *= 0.9

    if ball.xcor() > 380:
        ball.move_speed = 0.1
        ball.goal()
        scoreboard.r_point()

    if ball.xcor() < -380:
        ball.move_speed = 0.1
        ball.goal()
        scoreboard.l_point()




screen.exitonclick()