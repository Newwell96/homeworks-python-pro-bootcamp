from turtle import Turtle, Screen


MOVE_DISTANCE = 10
#ACCELERATE_COEFFICIENT = 2


class Ball(Turtle):

    def __init__(self):
        super().__init__()
        screen = Screen()
        self.penup()
        self.color("white")
        self.shape("circle")
        self.shapesize(stretch_wid=1, stretch_len=1)
        self.move_speed = 0.1
        self.x_move = MOVE_DISTANCE
        self.y_move = MOVE_DISTANCE

    def move(self):
        new_x = self.xcor() + self.x_move
        new_y = self.ycor() + self.y_move
        self.goto(new_x, new_y)

    def y_bounce(self):
        self.y_move = self.y_move*-1

    def x_bounce(self):
        self.x_move = self.x_move * -1

    def goal(self):
        self.home()
        self.x_bounce()

    #def accelerate(self):
    #    self.x_move = self.x_move * ACCELERATE_COEFFICIENT
    #    self.y_move = self.y_move * ACCELERATE_COEFFICIENT
#
    #def reset_speed(self):
    #   self.x_move = self.x_move / self.x_move * MOVE_DISTANCE
    #   self.y_move = self.y_move / self.y_move * MOVE_DISTANCE











