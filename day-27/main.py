from tkinter import *

window = Tk()

#window.title("My First GUI Program")
#window.minsize(width=500, height=300)
##Label
#my_label = Label(text="I am a Label", font=("Arial",24, "bold"))
#my_label["text"] = "New Text"
#my_label.config(text="New Text")
#my_label.grid(column=0,row=0)
#
##Button
#def button_clicked():
#    print("I got clicked")
#    #my_label.config(text=input.get())
#
#button = Button(text= "Click Me", command=button_clicked)
#button.grid(column=1,row=1)
#
#new_button = Button(text= "Click Me")
#new_button.grid(column=2,row=0)
#
#
##Entry
#
#input = Entry(width=50)
#print(input.get())
#input.grid(column=3,row=2)

window.title("Mile to Km Converter")
window.minsize(width=200, height=100)

equal = Label(text="is equal to", font=("Arial",12, "bold"))
equal.grid(column=0,row=1)

entry = Entry(width=10)
entry.grid(column=1,row=0)

result = Label(text="0", font=("Arial",12, "bold"))
result.grid(column=1,row=1)

def button_clicked():
    miles = float(entry.get())
    km = miles * 1.609
    result.config(text=f"{km}")

button = Button(text="Calculate", command=button_clicked)
button.grid(column=1,row=2)

miles = Label(text="Miles", font=("Arial",12, "bold"))
miles.grid(column=2,row=0)

Km = Label(text="Km", font=("Arial",12, "bold"))
Km.grid(column=2,row=1)

window.mainloop()