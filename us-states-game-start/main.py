import turtle
import pandas

screen = turtle.Screen()
screen.title("U.S. States Game")
image = "blank_states_img.gif"
screen.addshape(image)
turtle.shape(image)

data = pandas.read_csv("50_states.csv")
states_list = data["state"].to_list()
count_correct_states = 0
count_states = len(states_list)

game_on = True
while game_on:
    answer_state = screen.textinput(title=f"{count_correct_states}/{count_states} States Correct",
                                    prompt="What's another state's name?").title()
    if answer_state in states_list:
        states_list.remove(answer_state)
        count_correct_states += 1
        state_data = data[data.state == answer_state]
        state = turtle.Turtle()
        state.hideturtle()
        state.penup()
        state.goto(int(state_data.x), int(state_data.y))
        state.write(answer_state, True, align="center", font=("TimesNewRoman", 10 , "normal"))

    if count_correct_states == 50 or answer_state == "Exit":
        game_on = False
        x_cor = [int(state_data.x) for lost_state in states_list]
        y_cor = [int(state_data.y) for lost_state in states_list]
        for lost_state in states_list:
            state_data = data[data.state == lost_state]
            x_cor.append(int(state_data.x))
            y_cor.append(int(state_data.y))
        state_dict = {
            "state": states_list,
            "x": x_cor,
            "y": y_cor
        }
        lost_data = pandas.DataFrame(state_dict)
        lost_data.to_csv("lost_data.csv")




