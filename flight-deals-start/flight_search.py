import os
import requests
ENDPOINT = "https://tequila-api.kiwi.com"
API_KEY = os.environ["API_KEY"]

class FlightSearch:
    #This class is responsible for talking to the Flight Search API.
    def search_flight(self,city_name):
        location_endpoint = f"{ENDPOINT}/locations/query"
        headers = {

            "apikey": API_KEY,
        }
        query = {
            "term": city_name,

            "location_types": "city",
        }

        response = requests.get(url=location_endpoint, params=query, headers=headers)
        result = response.json()["locations"]


        code = result[0]["code"]
        return code


    #pass

