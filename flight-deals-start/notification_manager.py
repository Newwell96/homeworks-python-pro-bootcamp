import os
import requests
SMS_ENDPOINT = "https://semysms.net/api/3/sms.php"
TOKEN = os.environ.get("SMS_TOKEN")
class NotificationManager:
    #This class is responsible for sending notifications with the deal flight details.
    parameters = {
        "token" = TOKEN,
        "device" = "",
        "phone" = "",
        "msg" = "",
    }
    response = requests.post(url="")