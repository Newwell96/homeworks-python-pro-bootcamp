import os

import requests
from pprint import pprint


ENDPOINT = "https://api.tequila.kiwi.com/v2/search"
API_KEY = os.environ["API_KEY"]

class FlightData:
    #This class is responsible for structuring the flight data.
    def __init__(self):
        self.list_tickets = []
    def find_flight(self, arrival_city_code, today, departure_date, price):

        departure_airport_code = "LON"
        departure_city = "London"

        headers = {
            "apikey": API_KEY,
        }

        search = {
            "fly_from": departure_airport_code,

            "fly_to": arrival_city_code,

            "date_from": today,
            "date_to": departure_date,
            "nights_in_dst_from": 7,
            "nights_in_dst_to": 28,
            "flight_type": "round",
            "one_for_city": 1,
            "max_stopovers": 0,
            "curr": "GBP",
            "price_to": price,


        }
        response = requests.get(url=ENDPOINT, headers=headers, params=search)
        try:
            result = response.json()["data"][0]
            city = result["cityTo"]
            cost = result["price"]
            #pprint(result)
            text = f"{city}: £{cost}"
            print(text)
            self.list_tickets.append(text)
        except IndexError:
            print(f"No flights found for {result['cityTo']}.")
            return None

    #pass