#This file will need to use the DataManager,FlightSearch, FlightData, NotificationManager classes to achieve the program requirements.
from pprint import pprint

import flight_data
from data_manager import DataManager
from  flight_search import FlightSearch
from flight_data import FlightData
import datetime as dt


data_manager = DataManager()
flight_search = FlightSearch()
flight_data = FlightData()


data_manager.get_data()
sheet_data = data_manager.destination_data
#pprint(sheet_data)
#
for row in sheet_data:
    if row["iataCode"] == "":
        row["iataCode"] = flight_search.search_flight(row["city"])
#
#
#pprint(sheet_data)
data_manager.destination_data = sheet_data
data_manager.update_data(sheet_data)

now = dt.datetime.now()
half_year = now + dt.timedelta(days=6*30)
now = now.strftime("%d/%m/%Y")
half_year = half_year.strftime("%d/%m/%Y")

#print(now)
#print(half_year)

for row in sheet_data:
    flight_data.find_flight(arrival_city_code=row["iataCode"],today=now, departure_date=half_year, price=row["lowestPrice"] )

