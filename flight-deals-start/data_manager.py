import requests
import pprint
ENDPOINT = "https://api.sheety.co/af5401b5f90c88c43a78ca6f94e860a0/pythonFlightDeals/prices"
class DataManager:
    #This class is responsible for talking to the Google Sheet.
    def __init__(self):
        self.destination_data = {}

    def get_data(self):
        response = requests.get(url=ENDPOINT)
        self.destination_data = response.json()['prices']
        return self.destination_data

    def update_data(self,new_data):

        for city in self.destination_data:
            sheet_data = {
                "price": {
                    "iataCode": city["iataCode"]
                }
            }
            response = requests.put(url=f"{ENDPOINT}/{city['id']}", json=sheet_data)



    #pass