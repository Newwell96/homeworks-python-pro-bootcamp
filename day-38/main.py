import os

import requests
from datetime import datetime
from requests.auth import HTTPBasicAuth

APP_ID = os.environ["APP_ID"]
API_KEY = os.environ["API_KEY"]


SHEET_USERNAME = os.environ["USERNAME"]
SHEET_PASSWORD = os.environ["PASSWORD"]

current_date = datetime.now()
date_day = current_date.strftime("%d/%m/%Y")
date_time = current_date.strftime("%X")

nutritionix_endpoint = "https://trackapi.nutritionix.com/v2/natural/exercise"

sheety_endpoint = os.environ["SHEET_ENDPOINT"]

headers = {
    "x-app-id": APP_ID,
    "x-app-key": API_KEY,
}

querry = input("Tell me which exercises you did: ")

sheet_headers = {

}

body = {
    "query": querry,
    "gender": "male",
    "weight_kg": 94,
    "height_cm": 180,
    "age": 26
}

response = requests.post(url=nutritionix_endpoint, headers=headers, json=body)
result = response.json()["exercises"]
print(result)

basic = HTTPBasicAuth(SHEET_USERNAME,SHEET_PASSWORD)



for exercise in range(len(result)):

    sheet_data = {
      "workout": {
    	"date": date_day,
    	"time": date_time,
        "exercise": result[exercise]["name"].title(),
        "duration" : result[exercise]["duration_min"],
        "calories" : result[exercise]["nf_calories"]
      }
    }
    print(sheet_data)
    sheet_response = requests.post(url=sheety_endpoint, json=sheet_data, auth=basic)