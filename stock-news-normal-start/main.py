import os
import smtplib

my_email = os.environ["MY_EMAIL"]
my_password = os.environ["MY_PASSWORD"]


import requests


STOCK_NAME = "TSLA"
COMPANY_NAME = "Tesla Inc"
stock_api_key = os.environ["STOCK_API_KEY"]
news_api_key = os.environ["NEWS_API_KEY"]

STOCK_ENDPOINT = "https://www.alphavantage.co/query"
NEWS_ENDPOINT = "https://newsapi.org/v2/everything"

SMS_ENDPOINT = "https://semysms.net/api/3/sms.php"


    ## STEP 1: Use https://www.alphavantage.co/documentation/#daily
# When stock price increase/decreases by 5% between yesterday and the day before yesterday then print("Get News").
stock_parameters = {
    "function": "TIME_SERIES_DAILY_ADJUSTED",
    "symbol": STOCK_NAME,
    "apikey": stock_api_key,
}
#TODO 1. - Get yesterday's closing stock price. Hint: You can perform list comprehensions on Python dictionaries. e.g. [new_value for (key, value) in dictionary.items()]

stock_response = requests.get(url=STOCK_ENDPOINT, params=stock_parameters)
stock_response.raise_for_status()
stock_data = stock_response.json()["Time Series (Daily)"]

close_data = [value for (key, value) in stock_data.items()]

yeasterday = close_data[0]

before_yeasterday = close_data[1]

yeasterday_stock = float(yeasterday["4. close"])
print(yeasterday_stock)

#TODO 2. - Get the day before yesterday's closing stock price

before_yeasterday_stock = float(before_yeasterday["4. close"])
print(before_yeasterday_stock)

#TODO 3. - Find the positive difference between 1 and 2. e.g. 40 - 20 = -20, but the positive difference is 20. Hint: https://www.w3schools.com/python/ref_func_abs.asp

difference = round(abs(yeasterday_stock - before_yeasterday_stock),2)
print(difference)

#TODO 4. - Work out the percentage difference in price between closing price yesterday and closing price the day before yesterday.

percentage_difference = round(difference / yeasterday_stock * 100,2)
print(f"{percentage_difference} %")

#TODO 5. - If TODO4 percentage is greater than 5 then print("Get News").

if percentage_difference > 5:
    up_down = "↑"
else:
    up_down = "↓"

    ## STEP 2: https://newsapi.org/ 
    # Instead of printing ("Get News"), actually get the first 3 news pieces for the COMPANY_NAME. 

#TODO 6. - Instead of printing ("Get News"), use the News API to get articles related to the COMPANY_NAME.
news_parameters = {
    "apiKey": news_api_key,
    "q" : COMPANY_NAME,

}
news_response = requests.get(url=NEWS_ENDPOINT, params=news_parameters)
news_response.raise_for_status()


#TODO 7. - Use Python slice operator to create a list that contains the first 3 articles. Hint: https://stackoverflow.com/questions/509211/understanding-slice-notation
news_data = news_response.json()["articles"][:3]

print(news_data)



    ## STEP 3: Use twilio.com/docs/sms/quickstart/python
    #to send a separate message with each article's title and description to your phone number. 

#TODO 8. - Create a new list of the first 3 article's headline and description using list comprehension.

list_articles = [f"{STOCK_NAME}: {up_down}{percentage_difference}%\nHeadline: {article['title']}.\nBrief: {article['description']}." for article in news_data]
print(list_articles)

#TODO 9. - Send each article as a separate message via Twilio.


#for article in list_articles:
#    with smtplib.SMTP("smtp.gmail.com") as connection:
#        connection.starttls()
#        connection.login(user=my_email, password=my_password)
#        connection.sendmail(
#            from_addr=my_email,
#            to_addrs="test.831@yahoo.com",
#            msg=article
#        )


#Optional TODO: Format the message like this: 
"""
TSLA: 🔺2%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
or
"TSLA: 🔻5%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
"""

