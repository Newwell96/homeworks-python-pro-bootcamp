import ctypes
import re
import os
import psutil
import winreg

#Определяем разрешение монитора
user32 = ctypes.windll.user32
user32.SetProcessDPIAware()
width = user32.GetSystemMetrics(0)
height = user32.GetSystemMetrics(1)

#Определяем путь к STEAM
def read_reg(ep, p = r"", k = ''):
    try:
        key = winreg.OpenKeyEx(ep, p)
        value = winreg.QueryValueEx(key,k)
        if key:
            winreg.CloseKey(key)
        return value[0]
    except Exception as e:
        return None
    return None

steam_path = str(read_reg(ep = winreg.HKEY_LOCAL_MACHINE, p = r"SOFTWARE\Wow6432Node\Valve\Steam", k = 'InstallPath'))+r"\steam.exe"
steamapps_path = steam_path.replace("steam.exe", "")

#Берем данные из конфига игры, заменяем разрешение и перезаписываем конфиг
with open (fr'{steamapps_path}steamapps/common/Underlords/game/dac/cfg/video.txt', 'r') as file:
    config = file.read()

    new_res = re.sub(r'(\"setting.defaultres\"\s*)\"\d+\"', fr'\1"{width}"', config)
    new_res = re.sub(r'(\"setting.defaultresheight\"\s*)\"\d+\"', fr'\1"{height}"', new_res)

with open (fr'{steamapps_path}\steamapps\common\Underlords\game\dac\cfg\video.txt', 'w') as file:
    file.write(new_res)

#Проверяем запущен ли STEAM
process = "steam.exe" in (i.name() for i in psutil.process_iter())
print(process)

#Запускаем игру, если STEAM включен, в противном случае запускаем STEAM.
if process == True:
    os.startfile(rf'{steamapps_path}steamapps\common\Underlords\game\bin\win64\underlords.exe')
else:
    os.startfile(rf'{steam_path}')


